package com.example.myapplication

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth

class RegisterActivity : AppCompatActivity() {

    private lateinit var newEmail : EditText
    private lateinit var newPassword : EditText
    private lateinit var confirmPassword : EditText
    private lateinit var createButton: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        init()

        createButton.setOnClickListener {

            val mail = newEmail.text.toString()
            val password = newPassword.text.toString()
            val confirm = confirmPassword.text.toString()

            if (mail.isEmpty() || password.isEmpty() || confirm.isEmpty()) {
                Toast.makeText(this, "Something went wrong!", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }else if (password != confirm) {
                Toast.makeText(this, "Please try again!", Toast.LENGTH_SHORT).show()
            }

            FirebaseAuth.getInstance().createUserWithEmailAndPassword(mail, password).addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    startActivity(Intent(this, LoginActivity::class.java))
                    finish()
                }else {
                    Toast.makeText(this, "User already exists!", Toast.LENGTH_SHORT).show()
                }
            }

        }

    }

    private fun init() {
        newEmail = findViewById(R.id.newEmail)
        newPassword = findViewById(R.id.newPassword)
        confirmPassword = findViewById(R.id.confirmPassword)
        createButton = findViewById(R.id.createButton)
    }

}