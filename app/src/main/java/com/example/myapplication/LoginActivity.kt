package com.example.myapplication

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.ContactsContract
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth

class LoginActivity : AppCompatActivity() {

    private lateinit var userEmail : EditText
    private lateinit var userPassword : EditText
    private lateinit var loginButton : Button
    private lateinit var registerButton: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (FirebaseAuth.getInstance().currentUser != null) {
            startActivity(Intent(this, ProfileActivity::class.java))
            finish()
        }

        setContentView(R.layout.activity_login)

        init()

        onClickListeners()

    }

    private fun init() {
        userEmail = findViewById(R.id.userEmail)
        userPassword = findViewById(R.id.userPassword)
        loginButton = findViewById(R.id.loginButton)
        registerButton = findViewById(R.id.registerButton)
    }

    private fun onClickListeners() {
        registerButton.setOnClickListener {

            startActivity(Intent(this, RegisterActivity::class.java))

        }
        loginButton.setOnClickListener {

            val email = userEmail.text.toString()
            val password = userPassword.text.toString()

            if (email.isEmpty() || password.isEmpty()) {
                Toast.makeText(this, "Something went wrong!", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            FirebaseAuth.getInstance()
                .signInWithEmailAndPassword(email, password)
                .addOnCompleteListener { task ->

                    if (task.isSuccessful) {
                        startActivity(Intent(this, ProfileActivity::class.java))
                        finish()
                    }
                    else {
                        Toast.makeText(this, "Please try again!", Toast.LENGTH_SHORT).show()
                    }

                }

        }
    }

}